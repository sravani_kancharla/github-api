
The solution consists of three files:
1. Index.html - Main web page.
2. style.css - Style sheet for html elements
3. script.txt - Javascript file which has all the logic and makes appropriate API calls


Steps to execute:
1. Copy all the files to the same directly
2. Change script.txt file extension to script.js 
3. Open index.html in a browser
4. Click on "Get Files" button to display list of files and their details (Name, size, Download link) from the pre-created Github repository
5. To upload a new file by add a commit meesage, select a new file and click "Upload". This will upload the file to teh Github repository and refreshes the list of files.


The solution utilizes two different Github APIs

1. Get list of contents from a Github repository
2. Upload a new file to a Github repository